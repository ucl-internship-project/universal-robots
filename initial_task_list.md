# UR robots: 
•	Create GIT repository 
•	Create Issues 
•	Create internship log 
•	Explore UR robots (get started) 
•	Install and test URSim 
•	Explore URScript  
•	Test socket communication with UR robot (Python example - LKLM) 
•	Test UR IO on a RPi (Python example - LKLM) 
•	Explore UR RTDE 
•	Create Python script to interface UR robot using RTDE 
•	Explore URCap 
•	Develop IO PCB and URCap for the UR (CB series) tool connector 
•	Develop serial communication via DI/DO  
•	Explore ROS  

***

## FESTO Robotiono: 
•	Explore 
•	Test robot hardware and software 
•	Develop new controller/interface 

***

## EDU robot: 
•	Explore
•	Test robot hardware and software 
•	Explore ROS  
