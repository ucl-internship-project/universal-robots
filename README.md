# Universal Robots internship project in UCL

![Universal Robots logo](Images/universal-robots-vector-logo.png "UR robot logo")
![UCL logo](Images/ucl_logo.png "UCL logo")  

***

## Short project description

Interfacing to a Universal Robots arm with the end result and main task being 
**to develop IO PCB and URCap for the UR (CB series) tool connector**  

***

### Guides for following

Making a guide in [RTDE folder](https://gitlab.com/ucl-internship-project/universal-robots/-/tree/master/RTDE)
for easy following of the **RTDE setup**, the folder also contains additional **RTDE** documentation and requirements.  

Making a guide in [ROS folder](https://gitlab.com/ucl-internship-project/universal-robots/-/tree/master/ROS)
for easy following of the **ROS setup**, the folder also contains additional **ROS** documentation and requirements.  

Explore UR robots for physical and simulated robot experience [videos in UR website](https://academy.universal-robots.com/free-e-learning/).

***

#### Members

Members working on the project:  
Adam Dolinajec (adam5520@edu.ucl.dk)  
Gytis Valatkevicius (gyti0045@edu.ucl.dk)  
