# This folder contains the information and guides required for installing and using RTDE(REAL-TIME DATA EXCHANGE).

[RTDE setup guide made by us](https://gitlab.com/ucl-internship-project/universal-robots/-/blob/master/RTDE/RTDE_Guide_Ubuntu.pdf) - it was made following all the documentation we could find about RTDE  
and by the help from the following guide: 
[RTDE setup guide from sdu robotics](https://sdurobotics.gitlab.io/ur_rtde/index.html).

***

This is a guide on how to use the data synchronization protocol of the UR controller:  
[RTDE guide from Universal Robots for URCap and UR+](https://www.universal-robots.com/articles/ur/interface-communication/real-time-data-exchange-rtde-guide/)  
The RTDE synchronize external executables with the UR controller, for instance URCaps, without breaking any real-time properties.  
This document first describes the protocol and then provides a Python client reference implementation.

***

Prototype for IO control of the robot/simulator image:
![Prototype for IO control](/Images/prototype.jpg)  

Will be adding a circuit diagram soon, the script contains comments for explanation.  

prototype_IO_control.py is the script used for our prototype.  

## Will be adding more info/examples about URCap and other usage.
