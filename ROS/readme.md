# This folder contains the information and guides required for installing and using ROS(Robot Operating System).

Robot Operating System (ROS or ros) is an open source robotics middleware suite.  
Although ROS is not an operating system but a collection of software frameworks for robot software development, 
it provides services designed for a heterogeneous computer cluster such as hardware abstraction, low-level device control, 
implementation of commonly used functionality, message-passing between processes, and package management.
[ROS explanation from Wikipedia](https://en.wikipedia.org/wiki/Robot_Operating_System)

***

[ROS official site](https://www.ros.org/) contains the downloads needed for ROS and its setup, as well as getting started guides,
explanations and all the open-source documentation available.

## Will be adding more info/examples about usage.