ESP setup document

***

Contains the code for setting up the ESP32-s_cam webserver with the esp32 wifi chip.  

![ESP32 cam image](Images/ESP32cam.jpg "ESP cam photo")  

Note: the camera can be used and the stream can be viewed on the local webserver with just the 5V and GND pins used on the ESP32 module.
It is visible in the picture that I only use the power and gnd pins to pwoer the camera module.