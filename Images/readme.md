# This folder serves as a placeholder for images/pictures within use of the guides or gitlab project itself.

## Prototype images

![Prototype image](prototype.jpg)  

* Be aware!
The GPIO pins on the diagram might not fully represent the GPIO pins used 
in the script for I/O control.  

![Prototype-RPi diagram on paper](prototype_diagram.jpg)
![Prototype-RPi GPIO setup](prototype_rpi_gpio_setup.jpg)

* Pull-up resistors are after switches in the image(marked with 10K), their value is 10k.

* The other resistors depend on the LED's, for safety with RPi 1k resistors are safe,
but not the best for shining the LED's strong as possible.
