#Made for the prototype of RTDE I/O control of the UR with RPi GPIO pins
#Includes status LED's and the control of UR I/O
#Author: Gytis Valatkevicius

import RPi.GPIO as GPIO
from time import sleep
from rtde_io import RTDEIOInterface as RTDEIO
from rtde_receive import RTDEReceiveInterface as RTDEReceive

GPIO.setmode(GPIO.BCM)     

#configure the simulator/robot IP(has to be static)
rtde_io_ = RTDEIO("192.168.0.246")
rtde_receive_ = RTDEReceive("192.168.0.246")

#button power pins
GPIO.setup(24, GPIO.OUT) 
GPIO.setup(23, GPIO.OUT)

#digitalout status led pins
GPIO.setup(17, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)

#inputs from buttons pressed
GPIO.setup(27, GPIO.IN) 
GPIO.setup(22, GPIO.IN)

#button gpio output = high to wait for button press
GPIO.output(24, True)
GPIO.output(23, True)

try:  
    while True:            # this loop will carry on until you hit CTRL+C  
        #the 2 button I/O control
        if GPIO.input(22): # if GPIO 22 is high  
            print("Digital Output (7) is 1/HIGH while button is pressed") 
            rtde_io_.setStandardDigitalOut(7, True)
            sleep(0.1)
        else:  
            print("Digital Output (7) is 0/LOW - button not pressed") 
            rtde_io_.setStandardDigitalOut(7, False)
        sleep(1)         # wait time can be changed
        
        if GPIO.input(27): # if GPIO 27 is high  
            print("Tool Digital Output (0) is 1/HIGH while button is pressed")  
            rtde_io_.setToolDigitalOut(0, True)
            sleep(0.1)
        else:  
            print("Tool Digital Output (0) is 0/LOW - button not pressed")  
            rtde_io_.setToolDigitalOut(0, False)
        sleep(1)         # wait time can be changed
        
        #the 2 status LED's
        if rtde_receive_.getDigitalOutState(1):
            print("Digital Output (1) is 1/HIGH")  
            GPIO.output(17, True)
            sleep(1)
        else:  
            print("Digital Output (1) is 0/LOW")
            GPIO.output(17, False) # for making the LED off when DO state is LOW
        sleep(1)         # wait time can be changed
        
        if rtde_receive_.getDigitalOutState(17): #DO(16) or DO(17) for digital tool out 0/1 state
            print("Tool Digital Output (1) is 1/HIGH")  
            GPIO.output(18, True)
            sleep(1)
        else:  
            print("Tool Digital Output (1) is 0/LOW")
            GPIO.output(18, False) # for making the LED off when DO(x) state is LOW
        sleep(1)         # wait time can be changed
        
except KeyboardInterrupt:  
    GPIO.output(23, False)
    GPIO.output(24, False)
    GPIO.output(17, False)
    GPIO.output(18, False)
    GPIO.cleanup()         # clean up after yourself  
