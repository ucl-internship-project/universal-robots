# This folder contains the script examples of URScript or the scripts we have with python language.

## The URScript Programming Language

[URScript manual](https://s3-eu-west-1.amazonaws.com/ur-support-site/18383/scriptmanual_en_1.3.pdf) should be followed
for research and debugging.  

URScript is the robot programming languange used to control the robot at the Script Level.  
Like any other programming language URScripthas variables, types, flow of control statements, function etc.  
In addition URScript has a number of built-in variables and functions which monitors and controls the I/O and the movements of the robot.

***

URScript could be used for our needs to monitor and control the I/O and movements of the robot instead of C++/Python.

***

The folder also contains our current python scripts we made/have for examples.