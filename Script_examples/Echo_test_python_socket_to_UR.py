#Echo client program
import socket

HOST = "192.168.56.1"
PORT = 30002
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((HOST,PORT))
s.send("set_digital_out(2,False)"+"\n")
data = s.recv(1024)
s.close()
print("Received",repr(data))
